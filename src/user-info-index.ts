import { RibbonCompositionImpl } from 'eikon-framework/office-api/utils/ribbon/ribbon-composition-impl';
import { RibbonDispatcherImpl } from 'eikon-framework/office-api/utils/ribbon/ribbon-dispatcher-impl';
import { UserInfoButton } from './ribbon/user-info-button';
import { OpenWebAppButton } from './ribbon/open-web-app-button';
import { OfficeContext } from 'eikon-framework/office-api/office-context';
import { OfficeModule } from 'eikon-framework/office-api/office-module';
import featureContainer, { InversifyConfig } from './inversify-config';
import { UserInfoFunctionDispatcher } from './user-info-function-dispatcher';
import { Types } from './types';

export default class UserInfoOfficeModule implements OfficeModule {
    initialize(context: OfficeContext): Promise<void> {
        InversifyConfig.configureContainer(context);
        const userInfoButton = featureContainer.get<UserInfoButton>(Types.UserInfoButton);
        const openWebAppButton = featureContainer.get<OpenWebAppButton>(Types.OpenWebAppButton);
        const ribbonDispatcher = new RibbonDispatcherImpl(new RibbonCompositionImpl([userInfoButton, openWebAppButton]));
        const functionDispatcher = featureContainer.get<UserInfoFunctionDispatcher>(Types.FunctionDispatcher);
        context.ribbon.register(ribbonDispatcher);
        context.function.registerDispatcher(functionDispatcher);
        return Promise.resolve();
    }
}
