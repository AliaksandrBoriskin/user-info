import { Container } from 'inversify';
import { OfficeContext } from 'eikon-framework/office-api/office-context';
import { Types } from './types';
import { UserInfoButton } from './ribbon/user-info-button';
import { UserInfoApp } from './user-info-app';
import { UserInfoFunctionDispatcher } from './user-info-function-dispatcher';
import { OpenWebAppButton } from './ribbon/open-web-app-button';
import { SettingsProvider } from 'eikon-framework/office-api/settings-provider';
import { ObjectScope } from 'eikon-framework/office-api/object-model/object-scope';
import { EventBus } from 'eikon-framework/office-api/event-bus';
import { NavigationService } from 'eikon-framework/office-api/navigation-service';

const featureContainer = new Container();
export default featureContainer;

export class InversifyConfig {
    static configureContainer(context: OfficeContext) {
        featureContainer
            .bind<UserInfoButton>(Types.UserInfoButton)
            .to(UserInfoButton)
            .inSingletonScope();
        featureContainer
            .bind<OpenWebAppButton>(Types.OpenWebAppButton)
            .to(OpenWebAppButton)
            .inSingletonScope();
        featureContainer
            .bind<UserInfoFunctionDispatcher>(Types.FunctionDispatcher)
            .to(UserInfoFunctionDispatcher)
            .inSingletonScope();
        featureContainer
            .bind<UserInfoApp>(Types.UserInfoApp)
            .to(UserInfoApp)
            .inSingletonScope();
        featureContainer
            .bind<ObjectScope>(Types.ObjectScope)
            .toConstantValue(context.scope);
        featureContainer
            .bind<SettingsProvider>(Types.SettingsProvider)
            .toConstantValue(context.settings);
        featureContainer
            .bind<EventBus>(Types.EventBus)
            .toConstantValue(context.eventBus);
        featureContainer
            .bind<NavigationService>(Types.NavigationService)
            .toConstantValue(context.navigator);
    }
}