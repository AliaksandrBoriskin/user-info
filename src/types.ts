export namespace Types {
    export const UserInfoButton = Symbol('UserInfoButton');
    export const UserInfoApp = Symbol('UserInfoApp');
    export const OpenWebAppButton = Symbol('OpenWebAppButton');
    export const ObjectScope = Symbol('ObjectScope');
    export const FunctionDispatcher = Symbol('FunctionDispatcher');
    export const SettingsProvider = Symbol('SettingsProvider');
    export const EventBus = Symbol('EventBus');
    export const NavigationService = Symbol('NavigationService');
}