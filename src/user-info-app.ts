import { ObjectScope } from 'eikon-framework/office-api/object-model/object-scope';
import { EventBus } from 'eikon-framework/office-api/event-bus';
import { NavigationService } from 'eikon-framework/office-api/navigation-service';
import { Types } from './types';
import { injectable, inject } from 'inversify';

@injectable()
export class UserInfoApp {
    private _windowId: string;
    private _onWindowClosed: any;

    constructor(
        @inject(Types.NavigationService) private _navigator: NavigationService,
        @inject(Types.ObjectScope) private _scope: ObjectScope,       
        @inject(Types.EventBus) private readonly _eventBus: EventBus,
    ) {
        this._windowId = '';
        this._eventBus.subscribe('/my_sample_channel', (channel: string, data: any) => {
            switch (data.action) {
                case 'getActiveCell': {
                    this._scope.runSafe((provider: any) => {
                        const excel = provider.Excel.get();
                        let activeCell = excel.activeCell;
                        this._eventBus.publish(data.responseChannel, activeCell.value2);
                    });
                }
            }
        });
        let self = this;
        this._onWindowClosed = (uuid: string) => {
            self.onWindowClosed(uuid);
        };
    }

    public async open(options: any): Promise<string> {
        let properties = [];
        if (options && options.parent) {
            let parent = options.parent;
            properties.push({ parent_window_handle: parent });
        }
        this._windowId = await this._navigator.launchApp('EikonOffice', 'http://localhost:10030/Apps/user-info-app/?vers=1.0.0', {
            Location: {
                width: 450,
                height: 200
            },
            properties: properties
        });
        this._navigator.addListener('app-window-closed', this._onWindowClosed);
        return this._windowId;
    }

    private onWindowClosed(uuid: string) {
        if (this._windowId === uuid) {
            this._eventBus.unsubscribe('/my_sample_channel', () => {
                this._navigator.removeListener('app-window-closed', this._onWindowClosed);
            });
        }
    }

}
