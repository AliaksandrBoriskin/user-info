import * as fs from 'fs';

export class ResourceManager {

    private _table: Map<string, string> = new Map<string, string>();

    async getImageBase64String(key: string): Promise<string> {

        return new Promise((resolve, reject) => {
            if (this._table.has(key)) {
                resolve(this._table.get(key));
            } else {
                fs.readFile(__dirname + '\\images\\' + key + '.png', {}, (err, data) => {
                    if (err) {
                        reject();
                    } else {
                        let image = data.toString('base64');
                        this._table.set(key, image);
                        resolve(image);
                    }
                });
            }
        });
    }
}

export default new ResourceManager();
