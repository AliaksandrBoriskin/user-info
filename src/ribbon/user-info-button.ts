import { RibbonElement } from 'eikon-framework/office-api/ribbon/ribbon-element';
import ResourceManager from '../resources/resource-manager';
import { Types } from '../types';
import { inject, injectable } from 'inversify';
import { ObjectScope } from 'eikon-framework/office-api/object-model/object-scope';
import { SettingsProvider } from 'eikon-framework/office-api/settings-provider';

@injectable()
export class UserInfoButton extends RibbonElement {
    public readonly id = 'EikonOffice_UserInfoButton';
    private _userFullName: string = '';

    constructor(
        @inject(Types.ObjectScope) private _scope: ObjectScope,
        @inject(Types.SettingsProvider) private _settings: SettingsProvider,
    ) {
        super();
        this.getUserInformation();
    }

    private getUserInformation(): void {
        this._settings.getValue('USERDETAILS.FULLNAME')
        .then((userName: string) => {
            this._userFullName = userName;
        })
        .catch((err: any) => {
            console.log('Get user fullname failed. ' + err);
        });
    }

    public onAction() {
        this._scope.runSafe((provider: any) => {
            const excel = provider.Excel.get();
            let activeCell = excel.activeCell;
            activeCell.value2 = this._userFullName;
        });
    }

    public getEnabled(): Promise<boolean> {
        return Promise.resolve(true);
    }

    public getVisible(): Promise<boolean> {
        return Promise.resolve(true);
    }

    public getLabel(): Promise<string> {
        return Promise.resolve('User Info');
    }

    public getImage(): Promise<string> {
        return ResourceManager.getImageBase64String('user-info-button');
    }
}
