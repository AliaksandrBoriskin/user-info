import { RibbonElement } from 'eikon-framework/office-api/ribbon/ribbon-element';
import ResourceManager from '../resources/resource-manager';
import { UserInfoApp } from '../user-info-app';
import { Types } from '../types';
import { inject, injectable } from 'inversify';
import { RibbonControl } from 'eikon-framework/office-api/ribbon/ribbon-control';

@injectable()
export class OpenWebAppButton extends RibbonElement {
    public readonly id = 'EikonOffice_OpenWebAppButton';

    constructor(
        @inject(Types.UserInfoApp) private _userInfoApp: UserInfoApp,
    ) {
        super();
    }

    public async onAction(control: RibbonControl) {
        this._userInfoApp.open({ parent: control.context.hwnd });
    }

    public getEnabled(): Promise<boolean> {
        return Promise.resolve(true);
    }

    public getVisible(): Promise<boolean> {
        return Promise.resolve(true);
    }

    public getLabel(): Promise<string> {
        return Promise.resolve('Open User App');
    }

    public getImage(): Promise<string> {
        return ResourceManager.getImageBase64String('open-web-app-button');
    }
}
