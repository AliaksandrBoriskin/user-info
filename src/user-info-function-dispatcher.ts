import { FunctionContext } from 'eikon-framework/office-api/function/function-context';
import { injectable, inject } from 'inversify';
import { SettingsProvider } from 'eikon-framework/office-api/settings-provider';
import { Types } from './types';

@injectable()
export class UserInfoFunctionDispatcher {
    private _userFullName: string = '';
    private _userId: string = '';
    private _userEmail: string = '';
    private _userLanguage: string = '';

    constructor(
        @inject(Types.SettingsProvider) private _settings: SettingsProvider
    ) {
        this.getUserInformation();
    }

    private getUserInformation(): void {
        Promise.all([
            this._settings.getValue('USERDETAILS.FULLNAME'),
            this._settings.getValue('USERDETAILS.UUID'),
            this._settings.getValue('USERDETAILS.PREFERREDLANGUAGE'),
            this._settings.getValue('USERDETAILS.EMAILADDRESS'),
        ])
            .then((results) => {
                const [userName, uuid, language, email] = results;
                this._userFullName = userName;
                this._userId = uuid;
                this._userEmail = language;
                this._userLanguage = email;
            });
    }

    public getUserInfo(context: FunctionContext) {
        const currentDate = new Date().getHours().toString() + ":" 
            + new Date().getMinutes().toString();

        context.setResult({
            callerValue: currentDate,
            results: [
                {
                    position: { row: 1, column: 0 },
                    data: [
                        [ this._userFullName, this._userId ],
                        [ this._userLanguage, this._userEmail ],

                    ]
                },
            ]
        });
    }
}
