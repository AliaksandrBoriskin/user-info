import { OfficeContext } from 'eikon-framework/office-api/office-context';
import { FunctionContext } from 'eikon-framework/office-api/function/function-context';
import { ObjectScope } from 'eikon-framework/office-api/object-model/object-scope';
import { SettingsProvider } from 'eikon-framework/office-api/settings-provider';
export declare class FunctionDispatcher {
    private context;
    private _scope;
    private _settings;
    constructor(context: OfficeContext, _scope: ObjectScope, _settings: SettingsProvider);
    GetUserInfo(context: FunctionContext): Promise<void>;
    GetStockPrice(context: FunctionContext): void;
    StreamStockPrice(context: FunctionContext): void;
}
