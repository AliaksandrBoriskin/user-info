import { OfficeContext } from 'eikon-framework/office-api/office-context';
import { OfficeModule } from 'eikon-framework/office-api/office-module';
export default class UserInfoOfficeModule implements OfficeModule {
    initialize(context: OfficeContext): Promise<void>;
}
