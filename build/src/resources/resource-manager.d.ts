export declare class ResourceManager {
    private _table;
    getImageBase64String(key: string): Promise<string>;
}
declare const _default: ResourceManager;
export default _default;
