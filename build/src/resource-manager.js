"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs"));
class ResourceManager {
    constructor() {
        this._table = new Map();
    }
    getImageBase64String(key) {
        return __awaiter(this, void 0, void 0, function* () {
            /* if (this._table.has(key)) {
                return Promise.resolve(this._table.get(key));
            } */
            return new Promise((resolve, reject) => {
                if (this._table.has(key)) {
                    resolve(this._table.get(key));
                }
                else {
                    fs.readFile(__dirname + '\\images\\' + key + '.png', {}, (err, data) => {
                        if (err) {
                            reject();
                        }
                        else {
                            let image = data.toString('base64');
                            this._table.set(key, image);
                            resolve(image);
                        }
                    });
                }
            });
        });
    }
}
exports.ResourceManager = ResourceManager;
exports.default = new ResourceManager();
//# sourceMappingURL=resource-manager.js.map