import { Container } from 'inversify';
import { OfficeContext } from 'eikon-framework/office-api/office-context';
declare const featureContainer: Container;
export default featureContainer;
export declare class InversifyConfig {
    static configureContainer(context: OfficeContext): void;
}
