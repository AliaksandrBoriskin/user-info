import { RibbonControl } from 'eikon-framework/office-api/ribbon/ribbon-control';
import { RibbonDispatcherImpl } from 'eikon-framework/office-api/utils/ribbon/ribbon-dispatcher-impl';
export declare class UserInfoRibbonDispatcher extends RibbonDispatcherImpl {
    onAction(ribbonControl: RibbonControl): void;
    getLabel(ribbonControl: RibbonControl): Promise<string>;
    getEnabled(ribbonControl: RibbonControl): Promise<boolean>;
    getVisible(ribbonControl: RibbonControl): Promise<boolean>;
}
