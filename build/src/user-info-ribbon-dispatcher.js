"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ribbon_dispatcher_impl_1 = require("eikon-framework/office-api/utils/ribbon/ribbon-dispatcher-impl");
class UserInfoRibbonDispatcher extends ribbon_dispatcher_impl_1.RibbonDispatcherImpl {
    onAction(ribbonControl) {
        super.onAction(ribbonControl);
    }
    getLabel(ribbonControl) {
        if (ribbonControl.id === 'EikonOffice_UserInfoButton') {
            return Promise.resolve('User Info');
        }
        if (ribbonControl.id === 'EikonOffice_OpenWebAppButton') {
            return Promise.resolve('Open User App');
        }
        return super.getLabel(ribbonControl);
    }
    getEnabled(ribbonControl) {
        if (ribbonControl.id === 'EikonOffice_UserInfoButton') {
            return Promise.resolve(true);
        }
        if (ribbonControl.id === 'EikonOffice_OpenWebAppButton') {
            return Promise.resolve(true);
        }
        return Promise.reject();
    }
    getVisible(ribbonControl) {
        if (ribbonControl.id === 'EikonOffice_UserInfoButton') {
            return Promise.resolve(true);
        }
        if (ribbonControl.id === 'EikonOffice_OpenWebAppButton') {
            return Promise.resolve(true);
        }
        return Promise.reject();
    }
}
exports.UserInfoRibbonDispatcher = UserInfoRibbonDispatcher;
//# sourceMappingURL=user-info-ribbon-dispatcher.js.map