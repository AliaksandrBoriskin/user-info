import { FunctionContext } from 'eikon-framework/office-api/function/function-context';
import { SettingsProvider } from 'eikon-framework/office-api/settings-provider';
export declare class UserInfoFunctionDispatcher {
    private _settings;
    private _userFullName;
    private _userId;
    private _userEmail;
    private _userLanguage;
    constructor(_settings: SettingsProvider);
    private getUserInformation;
    getUserInfo(context: FunctionContext): void;
}
