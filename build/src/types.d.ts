export declare namespace Types {
    const UserInfoButton: unique symbol;
    const UserInfoApp: unique symbol;
    const OpenWebAppButton: unique symbol;
    const ObjectScope: unique symbol;
    const FunctionDispatcher: unique symbol;
    const SettingsProvider: unique symbol;
    const EventBus: unique symbol;
    const NavigationService: unique symbol;
}
