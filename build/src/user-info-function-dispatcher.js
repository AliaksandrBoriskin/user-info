"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const types_1 = require("./types");
let UserInfoFunctionDispatcher = class UserInfoFunctionDispatcher {
    constructor(_settings) {
        this._settings = _settings;
        this._userFullName = '';
        this._userId = '';
        this._userEmail = '';
        this._userLanguage = '';
        this.getUserInformation();
    }
    getUserInformation() {
        Promise.all([
            this._settings.getValue('USERDETAILS.FULLNAME'),
            this._settings.getValue('USERDETAILS.UUID'),
            this._settings.getValue('USERDETAILS.PREFERREDLANGUAGE'),
            this._settings.getValue('USERDETAILS.EMAILADDRESS'),
        ])
            .then((results) => {
            const [userName, uuid, language, email] = results;
            this._userFullName = userName;
            this._userId = uuid;
            this._userEmail = language;
            this._userLanguage = email;
        });
    }
    getUserInfo(context) {
        const currentDate = new Date().getHours().toString() + ":"
            + new Date().getMinutes().toString();
        context.setResult({
            callerValue: currentDate,
            results: [
                {
                    position: { row: 1, column: 0 },
                    data: [
                        [this._userFullName, this._userId],
                        [this._userLanguage, this._userEmail],
                    ]
                },
            ]
        });
    }
};
UserInfoFunctionDispatcher = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(types_1.Types.SettingsProvider))
], UserInfoFunctionDispatcher);
exports.UserInfoFunctionDispatcher = UserInfoFunctionDispatcher;
//# sourceMappingURL=user-info-function-dispatcher.js.map