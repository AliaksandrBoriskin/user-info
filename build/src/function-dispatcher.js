"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const types_1 = require("./types");
let FunctionDispatcher = class FunctionDispatcher {
    constructor(context, _scope, _settings) {
        this.context = context;
        this._scope = _scope;
        this._settings = _settings;
        this.context.function.registerDispatcher(this);
    }
    GetUserInfo(context) {
        return __awaiter(this, void 0, void 0, function* () {
            const userName = yield this._settings.getValue('USERDETAILS.FULLNAME');
            const userId = yield this._settings.getValue('USERDETAILS.UUID');
            const userLanguage = yield this._settings.getValue('USERDETAILS.PREFERREDLANGUAGE');
            const userEmail = yield this._settings.getValue('USERDETAILS.EMAILADDRESS');
            const currentDate = new Date().getMinutes() + ":" + new Date().getSeconds();
            context.setResult({
                callerValue: currentDate,
                results: [
                    {
                        position: { row: 1, column: 0 },
                        data: [
                            [userName, userId],
                            [userLanguage, userEmail],
                        ]
                    },
                ]
            });
        });
    }
    GetStockPrice(context) {
        let timeoutId = setTimeout(() => {
            let price = Math.floor(Math.random() * (+200 - +100)) + +100;
            context.setResult({ callerValue: price, totalColumnsCount: 1, totalRowsCount: 1 });
        }, 3000);
        context.onCanceled = () => {
            clearInterval(timeoutId);
        };
    }
    StreamStockPrice(context) {
        let timerId = setInterval(() => {
            let price = Math.floor(Math.random() * 100) + 100;
            context.setResult({ callerValue: price, totalColumnsCount: 1, totalRowsCount: 1 });
        }, 3000);
        context.onCanceled = () => {
            clearInterval(timerId);
        };
    }
};
FunctionDispatcher = __decorate([
    inversify_1.injectable(),
    __param(1, inversify_1.inject(types_1.Types.ObjectScope)),
    __param(2, inversify_1.inject(types_1.Types.SettingsProvider))
], FunctionDispatcher);
exports.FunctionDispatcher = FunctionDispatcher;
//# sourceMappingURL=function-dispatcher.js.map