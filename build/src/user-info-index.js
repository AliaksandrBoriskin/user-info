"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const ribbon_composition_impl_1 = require("eikon-framework/office-api/utils/ribbon/ribbon-composition-impl");
const ribbon_dispatcher_impl_1 = require("eikon-framework/office-api/utils/ribbon/ribbon-dispatcher-impl");
const inversify_config_1 = __importStar(require("./inversify-config"));
const types_1 = require("./types");
class UserInfoOfficeModule {
    initialize(context) {
        inversify_config_1.InversifyConfig.configureContainer(context);
        const userInfoButton = inversify_config_1.default.get(types_1.Types.UserInfoButton);
        const openWebAppButton = inversify_config_1.default.get(types_1.Types.OpenWebAppButton);
        const ribbonDispatcher = new ribbon_dispatcher_impl_1.RibbonDispatcherImpl(new ribbon_composition_impl_1.RibbonCompositionImpl([userInfoButton, openWebAppButton]));
        const functionDispatcher = inversify_config_1.default.get(types_1.Types.FunctionDispatcher);
        context.ribbon.register(ribbonDispatcher);
        context.function.registerDispatcher(functionDispatcher);
        return Promise.resolve();
    }
}
exports.default = UserInfoOfficeModule;
//# sourceMappingURL=user-info-index.js.map