"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const types_1 = require("./types");
const user_info_button_1 = require("./ribbon/user-info-button");
const user_info_app_1 = require("./user-info-app");
const user_info_function_dispatcher_1 = require("./user-info-function-dispatcher");
const open_web_app_button_1 = require("./ribbon/open-web-app-button");
const featureContainer = new inversify_1.Container();
exports.default = featureContainer;
class InversifyConfig {
    static configureContainer(context) {
        featureContainer
            .bind(types_1.Types.UserInfoButton)
            .to(user_info_button_1.UserInfoButton)
            .inSingletonScope();
        featureContainer
            .bind(types_1.Types.OpenWebAppButton)
            .to(open_web_app_button_1.OpenWebAppButton)
            .inSingletonScope();
        featureContainer
            .bind(types_1.Types.FunctionDispatcher)
            .to(user_info_function_dispatcher_1.UserInfoFunctionDispatcher)
            .inSingletonScope();
        featureContainer
            .bind(types_1.Types.UserInfoApp)
            .to(user_info_app_1.UserInfoApp)
            .inSingletonScope();
        featureContainer
            .bind(types_1.Types.ObjectScope)
            .toConstantValue(context.scope);
        featureContainer
            .bind(types_1.Types.SettingsProvider)
            .toConstantValue(context.settings);
        featureContainer
            .bind(types_1.Types.EventBus)
            .toConstantValue(context.eventBus);
        featureContainer
            .bind(types_1.Types.NavigationService)
            .toConstantValue(context.navigator);
    }
}
exports.InversifyConfig = InversifyConfig;
//# sourceMappingURL=inversify-config.js.map