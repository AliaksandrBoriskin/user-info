import { RibbonElement } from 'eikon-framework/office-api/ribbon/ribbon-element';
import { UserInfoApp } from '../user-info-app';
import { RibbonControl } from 'eikon-framework/office-api/ribbon/ribbon-control';
export declare class OpenWebAppButton extends RibbonElement {
    private _userInfoApp;
    readonly id = "EikonOffice_OpenWebAppButton";
    constructor(_userInfoApp: UserInfoApp);
    onAction(control: RibbonControl): Promise<void>;
    getEnabled(): Promise<boolean>;
    getVisible(): Promise<boolean>;
    getLabel(): Promise<string>;
    getImage(): Promise<string>;
}
