"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ribbon_element_1 = require("eikon-framework/office-api/ribbon/ribbon-element");
const resource_manager_1 = __importDefault(require("../resources/resource-manager"));
const types_1 = require("../types");
const inversify_1 = require("inversify");
let UserInfoButton = class UserInfoButton extends ribbon_element_1.RibbonElement {
    constructor(_scope, _settings) {
        super();
        this._scope = _scope;
        this._settings = _settings;
        this.id = 'EikonOffice_UserInfoButton';
        this._userFullName = '';
        this.getUserInformation();
    }
    getUserInformation() {
        this._settings.getValue('USERDETAILS.FULLNAME')
            .then((userName) => {
            this._userFullName = userName;
        })
            .catch((err) => {
            console.log('Get user fullname failed. ' + err);
        });
    }
    onAction() {
        this._scope.runSafe((provider) => {
            const excel = provider.Excel.get();
            let activeCell = excel.activeCell;
            activeCell.value2 = this._userFullName;
        });
    }
    getEnabled() {
        return Promise.resolve(true);
    }
    getVisible() {
        return Promise.resolve(true);
    }
    getLabel() {
        return Promise.resolve('User Info');
    }
    getImage() {
        return resource_manager_1.default.getImageBase64String('user-info-button');
    }
};
UserInfoButton = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(types_1.Types.ObjectScope)),
    __param(1, inversify_1.inject(types_1.Types.SettingsProvider))
], UserInfoButton);
exports.UserInfoButton = UserInfoButton;
//# sourceMappingURL=user-info-button.js.map