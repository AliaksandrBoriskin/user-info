import { RibbonElement } from 'eikon-framework/office-api/ribbon/ribbon-element';
import { ObjectScope } from 'eikon-framework/office-api/object-model/object-scope';
import { SettingsProvider } from 'eikon-framework/office-api/settings-provider';
export declare class UserInfoButton extends RibbonElement {
    private _scope;
    private _settings;
    readonly id = "EikonOffice_UserInfoButton";
    private _userFullName;
    constructor(_scope: ObjectScope, _settings: SettingsProvider);
    private getUserInformation;
    onAction(): void;
    getEnabled(): Promise<boolean>;
    getVisible(): Promise<boolean>;
    getLabel(): Promise<string>;
    getImage(): Promise<string>;
}
