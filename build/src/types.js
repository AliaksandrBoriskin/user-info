"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Types;
(function (Types) {
    Types.UserInfoButton = Symbol('UserInfoButton');
    Types.UserInfoApp = Symbol('UserInfoApp');
    Types.OpenWebAppButton = Symbol('OpenWebAppButton');
    Types.ObjectScope = Symbol('ObjectScope');
    Types.FunctionDispatcher = Symbol('FunctionDispatcher');
    Types.SettingsProvider = Symbol('SettingsProvider');
    Types.EventBus = Symbol('EventBus');
    Types.NavigationService = Symbol('NavigationService');
})(Types = exports.Types || (exports.Types = {}));
//# sourceMappingURL=types.js.map